# Changelog

## 1.0.0 (2022-03-25)


### Features

* **onepublish:** configure external url from environment first ([2e53592](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/2e53592920bc12b887e7e79221c3fb677d2a0bba))
* **onepublish:** get builder address from local interfaces ([23a167a](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/23a167a22b5865f1d8c5e9009ddb6c08e44b4ef4))
* **post-processor:** Adding opennebula publisher ([ffffe3f](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/ffffe3f220d6345bed59abd043016f198be4be36))
* **recipe:** adding alpine 3.15 falvour ([4ef2aec](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/4ef2aec365eaf17daedad84cace11a2a7bf6c537))
* **recipe:** Adding amon3 builder recipe ([4d46542](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/4d46542c6e28b4fcc7793f56cc5562ed7028993b))
* **recipe:** Adding eolebase recipe (k3d provisionned) ([2618b2b](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/2618b2b38f9dff4f0bc2cc5a520cc23094534b10))
* **recipe:** adding loki recipe with configuration support ([44a8274](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/44a8274aed0346831b27a4dd1f788e0c0df0438e)), closes [#22](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/issues/22)
* **recipe:** adding recipes for prometheus/loki/grafana service ([8be16d8](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/8be16d88e2f5f1bd48f8be23f0f36c126d796b15))
* **recipes:** Adding k3s recipe for alpine ([1a25da3](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/1a25da3953f17b5ed50ee5ce6fb4243cd6660f48))
* **recipe:** Use only one build source for all builds ([6558a4a](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/6558a4a5b519c33ce06aff6c4a995ff60a4311ef))
* **tools:** adding ERB templating support for services ([238741c](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/238741c17781abc30b6e789f5b2c03514de9d4f6))
* **tools:** adding image template publication support. ([d2b7611](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/d2b7611818935ce379acd561ae59dd65df405fc2))
* **tools:** adding one-template tool ([ae43e25](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/ae43e2574f212cfbf0c2e8484c2ec4d1b5f88440)), closes [#2](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/issues/2)
* **tools:** adding VM template publication support ([4022517](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/40225178b7e4f92c2e759455ec144b53aec7493a))
* **wrapper:** Adding an option to run only one build. ([8d5b1b9](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/8d5b1b9c970befce7c511bf883eeee58f4e866ed))


### Bug Fixes

* **alpine:** can't create grafana and prometheus ONE VM templates ([27ea79b](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/27ea79b8f7f3815f74b4ed77cd9f57bc5be85857))
* **debian:** the checksum URL does not work for archive ISO ([ab9440e](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/ab9440e8ef787b5a55458b88aaa68730c79328bb))
* **eolebase:** k3d does not work with multiple servers ([8b5f073](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/8b5f073c8b62aa031ab3c9976b41c3e5f0705c2b))
* **freebsd:** gitlab-runner image name is expected with full version ([b6691a3](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/b6691a3d31d36bcd907dbe056d723f5a46dfd70a))
* **gitlab-ci:** ubuntu has a `loki` variant ([51108f9](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/51108f90d286023cb60f2496b5888266ce67baff))
* **one-context:** growpart does not work for fr_FR.UTF-8 locales ([201d0f1](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/201d0f14d02a2888b7ba6420c62018fe02837fb6))
* **one-context:** update one-context version to 6.2 ([f528097](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/f5280971bb9a960af706c25727a87bbb3b6d7416))
* **one-templates:** packer wait CI timeout on OpenNebula exceptions ([26b6954](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/26b69545337f63e7f9b4b64fb39051c19e3967e6))
* **onepublish:** configuration file musn't be part of the repository ([721647c](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/721647c124a91d5cba555965bbc3ff69fc979db2))
* **onepublish:** configuration must come from environement first ([5e37104](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/5e37104d6200331a1e1a490c6effd386df9fa30b))
* **onepublish:** protect access to configuration values ([81b4ce6](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/81b4ce69b0a538ecbb6612c63d7ac53be0cd80a8))
* **post-processor:** disable warnings for onepublish ([d1c8825](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/d1c8825fc9ec746c25e42a6c27f2a269e9f8c404))
* **post-processor:** Fix bad image format ([927ec1f](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/927ec1f2c5af4e36f9ee174ec3ebedf8ff9fa205))
* **post-processor:** Removing hardcoded datastore ID ([6e331df](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/6e331df1af4608a99f4a58a010b7137b924dcdb1))
* **provisionning:** Checkout the develop branch ([ca3cd2b](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/ca3cd2b4d477b919f6a23c32cd7e0e9606f4d14d))
* **recipe:** adding disk_image option for debian flavours ([251f2a9](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/251f2a959589dd078447abc22fd5f6923e8ef13d)), closes [#16](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/issues/16)
* **recipe:** Adding wait time for nested vm builds ([90ccf18](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/90ccf1860753d0b648306e0d638ef170c19f121b))
* **recipe:** debian version is now 11.2 ([ae997cb](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/ae997cbb98c3d73a626e708deeb69da25cfb5086)), closes [#12](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/issues/12)
* **recipe:** do not serve http directory ([9eb398f](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/9eb398faec642fe92d4136e32d600c057a00671d)), closes [#14](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/issues/14)
* **recipe:** Fixing alpine recipes. ([6660712](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/6660712ed1260bcbcc19e0482e0e1a351b64e64f))
* **recipe:** Installing correct packer version. ([a449e73](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/a449e73967df8f0a67bdf44f5157ed837370fa74))
* **recipe:** moving disk_size to the flavour ([cfaee76](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/cfaee765638ae9655bfb3aa4a5da8bc58133b45a)), closes [#1](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/issues/1)
* **recipe:** skip the disk resize step ([f847ed7](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/f847ed78d668954ea83277dfcb0a3faa5b2b09f8)), closes [#15](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/issues/15)
* **recipe:** ubuntu do not manage disk size ([7499d3e](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/7499d3eff023e3ed676b8ec22a6c613ebfc248c7)), closes [#19](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/issues/19)
* **recipe:** Update Debian release version ([aee15a0](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/aee15a0d8c8bb3c35ad0b32b0a2026f261a59735))
* **recipe:** update output dir for k3s recipe ([766cc18](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/766cc18a11747e2c57f8560320356d9894573e2e))
* **recipe:** updating available memory for ubuntu builds. ([baf6722](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/baf6722a2ed07ab30092765fd26b519649708b3f)), closes [#9](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/issues/9)
* **recipe:** updating cloned branch for one service ([f97ba29](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/f97ba298ff14e25cf7d6580b6153a47db2a0cc5d))
* **recipe:** using ubuntu cloud images instead of iso ([4daba9f](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/4daba9fae03ed7c6f4a5a5a2aea083043a9f065f)), closes [#11](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/issues/11)
* **tools:** keep registration for service template update ([014567a](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/014567a8b836d0eb2d23f64172ecff2575d96bcf)), closes [#17](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/issues/17)
* **tools:** setting verbose mode to nil for cleaner logs ([0c487d1](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/0c487d1fb87cd6b03267025d58a9331a999cf139))
* **tools:** update chmodService call for service creation ([0e1ddb7](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/0e1ddb7e74d7e8066dea5712fb1a43e682d4deb3))
* **tools:** updating one-template service/vm/image publish ([6f88a29](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/6f88a2910541c01d246313270683bb47757b61f6))


### Styles

* **onepublish:** make rubocop happy ([3681972](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/3681972e68baf58c855639193954cde09d9f92f0))


### Continuous Integration

* **build:** generate base images ([481b7c8](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/481b7c84ad5400981dc11f600d5940d2435eb5c9))
* **build:** generate provisionned images ([6ae5367](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/6ae5367fd5b00ee82151f298129482db591d63ba))
* **new-ci:** integrate the new CI ([be28397](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/be2839761667ec79d4ae9c98de97b441843ebc6a))


### Code Refactoring

* **gitlab-ci:** use the factorized `Git.yaml` ([d38ef7d](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/d38ef7dfe5c3913d41742feb808888710b3a5aef))
* **gitlab-ci:** use the factorized `Semantic-release.yaml` ([d2f9189](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/d2f9189e4e319fa1e8b1cf2325708b65252338ac))
* **one-image:** `one-templates` tool replaces `onepublish.sh` ([427090a](https://gitlab.mim-libre.fr/EOLE/eole-3/infra/builder/commit/427090ae501608e3d194ace57c0e9bc0c069cd68))
