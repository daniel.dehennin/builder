build {
  name = "amon"
  description = <<EOF
This builder builds a QEMU image from the base build output.
The goal here is to install one-context and provide a few basic tools
to be used as a "OpenNebula cloud image" with it's provisionning.
EOF

  source "source.qemu.eole2" {
    output_directory = "${var.output_dir}/${var.eole_release}/provisionned/amon"
    vm_name          = "${local.output_name}-${var.eole_release}-amon.img"
    iso_url          = "${var.output_dir}/${var.eole_release}/base/${local.output_name}-${var.eole_release}.img"
    iso_checksum     = "none"
    disk_image       = true
    boot_command     = [ "<enter><enter><wait>" ]
  }

  provisioner "ansible" {
    playbook_file = "${path.cwd}/provisionning/${var.name}/playbooks/amon.yaml"
    ansible_env_vars = [
      "ANSIBLE_SSH_ARGS='-oHostKeyAlgorithms=+ssh-rsa -oPubkeyAcceptedKeyTypes=ssh-rsa'",
      "ANSIBLE_HOST_KEY_CHECKING=False"
    ]
    user="${local.ssh_user}"
    extra_arguments = [
      "--scp-extra-args", "'-O'",
      "--extra-vars", "VM_NAME=${local.output_name}",
      "--extra-vars", "HTTP_PROXY=${var.HTTP_PROXY} HTTPS_PROXY=${var.HTTPS_PROXY}",
      "--extra-vars", "http_proxy=${var.http_proxy} https_proxy=${var.https_proxy}",
      "--extra-vars", "eole_release=${var.eole_release}"
    ]
  }

  provisioner "file" {
    destination = "/tmp/one-context.sh"
    source      = "${path.cwd}/provisionning/debian/one-context.sh"
  }

  provisioner "shell" {
    inline = [
      "sh -cx 'sudo bash /tmp/one-context.sh'"
    ]
  }

  post-processor "shell-local" {
    inline = [
      "/bin/sh ${path.cwd}/post-processors/sparsify.sh ${var.output_dir}/${var.eole_release}/provisionned/amon ${var.image_version}",
    ]
  }
}