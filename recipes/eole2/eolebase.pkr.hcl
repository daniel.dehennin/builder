build {
  name = "eolebase"
  description = <<EOF
This builder builds a QEMU image from the base build output.
The goal here is to install one-context and provide a few basic tools
to be used as a "OpenNebula cloud image" with it's provisionning.
EOF

  source "source.qemu.eole2" {
    output_directory = "${var.output_dir}/${var.eole_release}/provisionned/eolebase"
    vm_name          = "${local.output_name}-${var.eole_release}-eolebase.img"
    iso_url          = "${var.output_dir}/${var.eole_release}/base/${local.output_name}-${var.eole_release}.img"
    iso_checksum     = "none"
    disk_image       = true
    boot_command     = [ "<enter><enter><wait>" ]
  }

  provisioner "file" {
    destination = "/tmp/one-context.sh"
    source      = "${path.cwd}/provisionning/debian/one-context.sh"
  }

  provisioner "shell" {
    inline = [
      "sh -cx 'sudo bash /tmp/one-context.sh'"
    ]
  }

  post-processor "shell-local" {
    inline = [
      "/bin/sh ${path.cwd}/post-processors/sparsify.sh ${var.output_dir}/${var.eole_release}/provisionned/eolebase ${var.image_version}",
      #"ruby ${path.cwd}/tools/one-templates -t image -T ${path.cwd}/templates/one/image/common.tpl -n ${local.output_name}-${var.eole_release}-${build.name} -c '${build.name} base image' --image-file ${var.output_dir}/${var.eole_version}/provisionned/${build.name}/${local.output_name}-${var.eole_release}-${build.name}.img",
      #"ruby ${path.cwd}/tools/one-templates -t vm -T ${path.cwd}/templates/one/vm/${build.name}.xml -n ${local.output_name}-${var.eole_release}-${build.name} --image-name ${local.output_name}-${var.eole_release}-${build.name}",
    ]
  }
}