variable "name" {
  type = string
  default = "debian"
}

variable "version" {
  type = string
  default = "9.10.0"
}

variable "short_version" {
  type = string
  default = "9"
}

variable "arch" {
  type = string
  default = "amd63"
}

variable "output_dir" {
  type    = string
  default = "output/debian"
}

variable "source_url" {
  type = string
  default = "https://cdimage.debian.org/cdimage/release"
}

variable "iso_cd_checksum" {
  type = string
  default = "sha256:ae6d563d2444665316901fe7091059ac34b8f67ba30f9159f7cef7d2fdc5bf8a"
}

variable "image_version" {
  type = string
  default = "0.0.1"
}

variable "code_name" {
  type = string
  default = "bullseye"
}

variable "boot_command" {
  type = list(string)
  default = []
}

variable "image_dir_name" {
  type = string
  default = "legacy-images"
}

variable "vm_hostname" {
  type = string
  default = "debian"
}

variable "cloud_init_runcmd" {
  type = list(string)
  default = [ "uname" ]
}
