#Flavour base
build {
  name = "base"
  description = <<EOF
This builder builds a QEMU image from an Alpine "virt" CD ISO file.
EOF

  source "qemu.alpine" {
    output_directory = "${var.output_dir}/${var.version}/base"
    vm_name          = "${local.output_name}-${var.version}.img"
    disk_size        = 8000
    iso_url          = "${local.source_iso}"
    iso_checksum     = "${var.iso_cd_checksum}"
    http_content = {
      "/ssh-packer-pub.key" = data.sshkey.install.public_key
      "/install.conf" = templatefile("${path.cwd}/templates/conf/${var.name}/install/awnsers.pktpl.hcl", local.installOpts)
    }
    boot_command = [
      "<wait5s>root<enter>",
      "<wait1s><enter>",
      "<wait1s>setup-interfaces<enter><wait1s><enter><wait1s><enter><wait1s><enter>",
      "<wait1s>ifup eth0<enter>",
      "<wait1s>mkdir -p .ssh<enter>",
      "<wait1s>wget http://{{.HTTPIP}}:{{.HTTPPort}}/ssh-packer-pub.key -O .ssh/authorized_keys<enter><wait1s>",
      "<wait1s>chmod 600 .ssh/authorized_keys<enter>",
      "<wait1s>wget http://{{.HTTPIP}}:{{.HTTPPort}}/install.conf<enter><wait1s>",
      "<wait1s>setup-sshd -c openssh -k .ssh/authorized_keys<enter><wait1s>",
    ]
  }

  provisioner "shell" {
    pause_before = "1s"
    expect_disconnect = true  # Because the previous step has rebooted the machine
    script = "${path.cwd}/provisionning/${var.name}/${var.name}-${var.short_version}-install.sh"
    valid_exit_codes = [ 0, 141 ]
  }

  provisioner "shell" {
    pause_before = "1s"
    inline = [ "sh -cx 'mkdir -p ${local.builder_config.TemplateDir}'" ]
  }

  provisioner "file" {
    destination = "/tmp/alpine-postinstall.sh"
    source      = "${path.cwd}/provisionning/${var.name}/${var.name}-${var.short_version}-postinstall.sh"
  }

  provisioner "shell" {
    inline = [
      "sh /tmp/alpine-postinstall.sh"
    ]
  }

  provisioner "file" {
    destination = "/etc/conf.d/chronyd"
    source = "${path.cwd}/templates/conf/${var.name}/conf.d/"
  }

  post-processor "manifest" {
    keep_input_artifact = true
  }

  post-processor "shell-local" {
    inline = [
      "/bin/sh ${path.cwd}/post-processors/sparsify.sh ${var.output_dir}/${var.version}/base ${var.image_version}",
      "ruby ${path.cwd}/tools/one-templates -t image -T ${path.cwd}/templates/one/image/common.tpl -n ${local.output_name}-${var.version} -c '${local.output_name}-${var.version} base image' --image-file ${var.output_dir}/${var.version}/base/${local.output_name}-${var.version}.img"
    ]
  }
}
