source qemu "alpine" {
  cpus = 1
  memory      = "${local.memory}"
  accelerator = "kvm"


  headless = true

  # Serve the `http` directory via HTTP, used for preseeding the Debian installer.
  http_port_min  = 9990
  http_port_max  = 9999

  # SSH ports to redirect to the VM being built
  host_port_min = 2222
  host_port_max = 2229

  # This user is configured in the preseed file.
  ssh_username     = "${local.ssh_user}"
  ssh_private_key_file = data.sshkey.install.private_key_path
  ssh_wait_timeout = "1000s"

  shutdown_command = "/sbin/poweroff"

  # Builds a compact image
  disk_compression   = true
  disk_discard       = "unmap"
  skip_compaction    = false
  disk_detect_zeroes = "unmap"

  format           = "qcow2"

  boot_wait = "5s"
}
