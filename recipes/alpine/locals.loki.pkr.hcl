locals {
  Config = {
    Name = "loki"
    ConfigFiles = [
      {
        destination = "/etc/loki/loki-local-config.yaml"
        source = "loki-local-config.pktpl.hcl"
        mod = "600"
      }
    ]
    AuthEnabled = false
    User = "loki"
    Group = "grafana"
    HTTPPort = "3100"
    GRPCPort = "9096"
    AlertManagerURL = "http://localhost:9093"
    StorageRoot = "/var/loki"
    SharedStore = "filesystem"
    ObjectStore = "filesystem"
    LogLevel = "error"
    S3 = {
        URL = ""
        BucketName = ""
        APIKey = ""
        APISecretKey = ""
    }
  }
}