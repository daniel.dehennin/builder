#Flavour ${build.name}
build {
  name = "${local.Config.Name}"
  description = <<EOF
This builder builds a QEMU image from the base build output. The goal here is to install loki
with it's provisionning.
EOF

  source "source.qemu.alpine" {
    output_directory = "${var.output_dir}/${var.version}/provisionned/${local.Config.Name}"
    vm_name          = "${local.output_name}-${var.version}-${local.Config.Name}.img"
    iso_url          = "${var.output_dir}/${var.version}/base/${local.output_name}-${var.version}.img"
    iso_checksum     = "none"
    disk_size        = 40960
    disk_image       = true
    boot_command     = [ "<enter><enter><wait>" ]
    ssh_clear_authorized_keys = true
  }

  provisioner "file" {
    destination = "/tmp/${build.name}.sh"
    source      = "${path.cwd}/provisionning/${var.name}/${build.name}.sh"
  }

  provisioner "file" {
    destination = "/tmp/install-${build.name}.sh"
    source      = "${path.cwd}/provisionning/${build.name}/install.sh"
  }

  provisioner "file" {
    destination = "/tmp/install-templater.sh"
    source      = "${path.cwd}/provisionning/templater/install.sh"
  }

  // Install OpenNebula context tool
  provisioner "file" {
    destination = "/tmp/one-context.sh"
    source      = "${path.cwd}/provisionning/${var.name}/one-context.sh"
  }

  // Deploy the opennebula context script to manage configuration
  provisioner "file" {
    destination = "/tmp/net-96-templater"
    source = "${path.cwd}/provisionning/one-context/net-96-templater"
  }

  provisioner "shell" {
    inline = [
      "sh -cx 'sh /tmp/one-context.sh'",
      "sh -cx 'sh /tmp/${build.name}.sh'",
      "sh -cx 'sh /tmp/install-templater.sh'",
      "sh -cx 'sh /tmp/install-${build.name}.sh'",
      "sh -cx 'cp /tmp/net-96-templater /etc/one-context.d/net-96-templater'",
      "sh -cx 'chmod +x /etc/one-context.d/net-96-templater'"
    ]
  }

  provisioner "file" {
    name = "templater"
    destination = "${local.Config.ConfigFiles[0].destination}"
    content = templatefile("${path.cwd}/templates/conf/${build.name}/${local.Config.ConfigFiles[0].source}", local.Config)
  }


  // Create Builder directories on the image.
  provisioner "shell" {
    inline = [
      "sh -cx 'mkdir -p ${local.builder_config.TemplateDir}/${build.name}'",
      "sh -cx 'chown ${local.Config.User}:${local.Config.Group} ${local.builder_config.TemplateDir}/${build.name}'",
      "sh -cx 'mkdir -p ${local.builder_config.ValueDir}/${build.name}'",
      "sh -cx 'chown ${local.Config.User}:${local.Config.Group} ${local.builder_config.ValueDir}/${build.name}'",
      "sh -cx 'mkdir -p ${local.Config.StorageRoot}'",
      "sh -cx 'chown ${local.Config.User}:${local.Config.Group} ${local.Config.StorageRoot}'" ]
  }

  // Copy configuration template on the image
  provisioner "file" {
    destination = "${local.builder_config.TemplateDir}/${build.name}/${local.Config.ConfigFiles[0].source}"
    source = "${path.cwd}/templates/conf/${build.name}/${local.Config.ConfigFiles[0].source}"
  }

  // Copy configuration values on the image
  provisioner "file" {
    destination = "${local.builder_config.ValueDir}/${build.name}/values.json"
    content = "${jsonencode(local.Config)}"
  }

  post-processor "shell-local" {
    name = "publish"
    inline = [
      "/bin/sh ${path.cwd}/post-processors/sparsify.sh ${var.output_dir}/${var.version}/provisionned/${build.name} ${var.image_version}",
      "ruby ${path.cwd}/tools/one-templates -t image -T ${path.cwd}/templates/one/image/common.tpl -n ${local.output_name}-${var.version}-${build.name} -c '${build.name} base image' --image-file ${var.output_dir}/${var.version}/provisionned/${build.name}/${local.output_name}-${var.version}-${build.name}.img",
      "ruby ${path.cwd}/tools/one-templates -t vm -T ${path.cwd}/templates/one/vm/${build.name}.xml -n ${local.output_name}-${var.version}-${build.name} --image-name ${local.output_name}-${var.version}-${build.name}",
    ]
  }

}