#Flavour prometheus
build {
  name = "prometheus"
  description = <<EOF
This builder builds a QEMU image from the base build output. The goal here is to install prometheus
with it's provisionning.
EOF

  source "source.qemu.alpine" {
    output_directory = "${var.output_dir}/${var.version}/provisionned/prometheus"
    vm_name          = "${local.output_name}-${var.version}-prometheus.img"
    iso_url          = "${var.output_dir}/${var.version}/base/${local.output_name}-${var.version}.img"
    iso_checksum     = "none"
    disk_size        = 40960
    disk_image       = true
    boot_command     = [ "<enter><enter><wait>" ]
  }

  provisioner "file" {
    destination = "/tmp/${build.name}.sh"
    source      = "${path.cwd}/provisionning/${var.name}/${build.name}.sh"
  }

  provisioner "file" {
    destination = "/tmp/one-context.sh"
    source      = "${path.cwd}/provisionning/${var.name}/one-context.sh"
  }

  provisioner "shell" {
    inline = [
      "sh -cx 'sh /tmp/one-context.sh'",
      "sh -cx 'sh /tmp/${build.name}.sh'"
    ]
  }

  post-processor "shell-local" {
    inline = [
      "/bin/sh ${path.cwd}/post-processors/sparsify.sh ${var.output_dir}/${var.version}/provisionned/prometheus ${var.image_version}",
      "ruby ${path.cwd}/tools/one-templates --type image --name ${local.output_name}-${var.version}-prometheus --comment 'Prometheus base image' --image-file ${var.output_dir}/${var.version}/provisionned/prometheus/${local.output_name}-${var.version}-prometheus.img",
      "ruby ${path.cwd}/tools/one-templates -t image -T ${path.cwd}/templates/one/image/common.tpl -n ${local.output_name}-${var.version}-prometheus -c 'prometheus base image' --image-file ${var.output_dir}/${var.version}/provisionned/prometheus/${local.output_name}-${var.version}-prometheus.img",
      "ruby ${path.cwd}/tools/one-templates -t vm -T ${path.cwd}/templates/one/vm/loki.xml -n ${local.output_name}-${var.version}-prometheus --image-name ${local.output_name}-${var.version}-prometheus",
    ]
  }

}
