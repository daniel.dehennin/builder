#Flavour eolebase
build {
  name = "eolebase"
  description = <<EOF
This builder builds a QEMU image from the base build output. The goal here is to install gitlab-runner
with it's provisionning.
EOF

  source "source.qemu.ubuntu" {
    output_directory = "${var.output_dir}/${var.version}/provisionned/eolebase"
    vm_name          = "${local.output_name}-${var.version}-eolebase.img"
    iso_url          = "${var.output_dir}/${var.version}/base/${local.output_name}-${var.version}.img"
    iso_checksum     = "none"
    disk_image       = true
    boot_command     = [ "<enter><enter><wait>" ]
  }

  provisioner "file" {
    destination = "/tmp/${build.name}.sh"
    source      = "${path.cwd}/provisionning/ubuntu/${build.name}.sh"
  }

  provisioner "file" {
    destination = "/tmp/one-context.sh"
    source      = "${path.cwd}/provisionning/debian/one-context.sh"
  }

  provisioner "shell" {
    inline = [
      "sh -cx 'sudo bash /tmp/${build.name}.sh'",
      "sh -cx 'sudo bash /tmp/one-context.sh'"
    ]
  }

  post-processor "shell-local" {
    inline = [
      "/bin/sh ${path.cwd}/post-processors/sparsify.sh ${var.output_dir}/${var.version}/provisionned/eolebase ${var.image_version}",
      "ruby ${path.cwd}/tools/one-templates -t image -T ${path.cwd}/templates/one/image/common.tpl -n ${local.output_name}-${var.version}-${build.name} -c 'Eolebase3 on ${local.output_name}-${var.version}' --image-file ${var.output_dir}/${var.version}/provisionned/eolebase/${local.output_name}-${var.version}-eolebase.img"
    ]
  }
}
