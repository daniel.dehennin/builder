#Flavour loki
build {
  name = "loki"
  description = <<EOF
This builder builds a QEMU image from the base build output. The goal here is to install loki
with it's provisionning.
EOF

  source "source.qemu.ubuntu" {
    output_directory = "${var.output_dir}/${var.version}/provisionned/loki"
    vm_name          = "${local.output_name}-${var.version}-loki.img"
    iso_url          = "${var.output_dir}/${var.version}/base/${local.output_name}-${var.version}.img"
    iso_checksum     = "none"
    disk_size        = 40960
    disk_image       = true
    boot_command     = [ "<enter><enter><wait>" ]
  }

  provisioner "file" {
    destination = "/tmp/${build.name}.sh"
    source      = "${path.cwd}/provisionning/${build.name}/install.sh"
  }

  provisioner "file" {
    destination = "/tmp/one-context.sh"
    source      = "${path.cwd}/provisionning/debian/one-context.sh"
  }

  provisioner "file" {
    destination = "/tmp/install-templater.sh"
    source      = "${path.cwd}/provisionning/templater/install.sh"
  }

  provisioner "shell" {
    inline = [
      "sh -cx 'sudo sh /tmp/one-context.sh'",
      "sh -cx 'sudo sh /tmp/install-templater.sh'",
      "sh -cx 'sudo sh /tmp/${build.name}.sh'"
    ]
  }

  provisioner "file" {
    destination = "/tmp/${build.name}-local-config.pktpl.hcl"
    source = "${path.cwd}/templates/conf/${build.name}/${build.name}-local-config.pktpl.hcl"
  }

  provisioner "file" {
    destination = "/tmp/net-96-templater"
    source = "${path.cwd}/provisionning/one-context/net-96-templater"
  }

  provisioner "file" {
    name = "templater"
    destination = "/tmp/${build.name}.config"
    content = templatefile("${path.cwd}/templates/conf/${build.name}/${local.Config.ConfigFiles[0].source}", local.Config)
  }

  provisioner "shell" {
    inline = [
      "sh -cx 'sudo cp /tmp/net-96-templater /etc/one-context.d/net-96-templater'",
      "sh -cx 'sudo chmod +x /etc/one-context.d/net-96-templater'",
      "sh -cx 'sudo cp /tmp/${build.name}.config ${local.Config.ConfigFiles[0].destination}'",
      "sh -cx 'sudo mkdir -p ${local.builder_config.TemplateDir}/${build.name}'",
      "sh -cx 'sudo chown ${local.Config.User}:${local.Config.Group} ${local.builder_config.TemplateDir}/${build.name}'",
      "sh -cx 'sudo mkdir -p ${local.builder_config.ValueDir}/${build.name}'",
      "sh -cx 'sudo chown ${local.Config.User}:${local.Config.Group} ${local.builder_config.ValueDir}/${build.name}'",
      "sh -cx 'sudo mkdir -p ${local.Config.StorageRoot}'",
      "sh -cx 'sudo chown ${local.Config.User}:${local.Config.Group} ${local.Config.StorageRoot}'" ]
  }

  post-processor "shell-local" {
    inline = [
      "/bin/sh ${path.cwd}/post-processors/sparsify.sh ${var.output_dir}/${var.version}/provisionned/${build.name} ${var.image_version}",
      "ruby ${path.cwd}/tools/one-templates -t image -T ${path.cwd}/templates/one/image/common.tpl -n ${local.output_name}-${var.version}-${build.name} -c '${build.name} base image' --image-file ${var.output_dir}/${var.version}/provisionned/${build.name}/${local.output_name}-${var.version}-${build.name}.img",
      "ruby ${path.cwd}/tools/one-templates -t vm -T ${path.cwd}/templates/one/vm/${build.name}.xml -n ${local.output_name}-${var.version}-${build.name} --image-name ${local.output_name}-${var.version}-${build.name}",
    ]
  }

}