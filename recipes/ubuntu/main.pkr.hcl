#Flavour base
build {
  name = "base"
  description = <<EOF
This builder builds a QEMU image from an Ubuntu cloud image.
EOF

  source "qemu.ubuntu" {
    output_directory = "${var.output_dir}/${var.version}/base"
    vm_name          = "${local.output_name}-${var.version}.img"
    boot_command     = var.boot_command
    iso_url          = "${local.source_iso}"
    iso_checksum     = "${local.source_checksum}"
    disk_image       = true
    disk_size        = 10240
    cd_content = {
      "meta-data" = jsonencode(local.instance_data)
      "user-data" = templatefile("${path.cwd}/templates/conf/cloud-init/user-data",
        { user = local.ssh_user,
          password = local.ssh_password,
          runcmd = var.cloud_init_runcmd })
    }
    cd_label =  "cidata"
  }

  provisioner "file" {
    destination = "/tmp/ubuntu.sh"
    source      = "${path.cwd}/provisionning/${var.name}/${var.name}.sh"
  }

  provisioner "shell" {
    inline = [
      "sh -cx 'sudo bash /tmp/ubuntu.sh ${var.vm_hostname}'"
    ]
  }

  post-processor "shell-local" {
    inline = [
      "/bin/sh ${path.cwd}/post-processors/sparsify.sh ${var.output_dir}/${var.version}/base ${var.image_version}",
      "ruby ${path.cwd}/tools/one-templates -t image -T ${path.cwd}/templates/one/image/common.tpl -n ${local.output_name}-${var.version} -c '${local.output_name}-${var.version} base image' --image-file ${var.output_dir}/${var.version}/base/${local.output_name}-${var.version}.img"
    ]
  }

  post-processor "manifest" {
    keep_input_artifact = true
  }
}
