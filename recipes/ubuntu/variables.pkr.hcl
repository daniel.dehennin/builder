variable "name" {
  type = string
  default = "ubuntu"
}

variable "version" {
  type = string
  default = "9.10.0"
}

variable "short_version" {
  type = string
  default = "9"
}

variable "arch" {
  type = string
  default = "amd64"
}

variable "output_dir" {
  type    = string
  default = "output/ubuntu"
}

variable "source_url" {
  type = string
  default = "https://cdimage.debian.org/cdimage/release"
}

variable "iso_cd_checksum" {
  type = string
  default = "sha256:00a59da086ba5167c7ecb21aed308f73979f67ee9c6b4981662ad469e540d779"
}

variable "image_version" {
  type = string
  default = "0.0.1"
}

variable "code_name" {
  type = string
  default = "focal"
}

variable "boot_command" {
  type = list(string)
  default = []
}

variable "image_dir_name" {
  type = string
  default = "legacy-images"
}

variable "vm_hostname" {
  type = string
  default = "ubt"
}

variable "cloud_init_runcmd" {
  type = list(string)
  default = [ "uname" ]
}
