#Flavour base-onecontext
build {
  name = "base-onecontext"
  description = <<EOF
This builder builds a QEMU image from the base build output.
The goal here is to install one-context and provide a few basic tools
to be used as a "OpenNebula cloud image" with it's provisionning.
EOF

  source "source.qemu.ubuntu" {
    output_directory = "${var.output_dir}/${var.version}/provisionned/one-context"
    vm_name          = "${local.output_name}-${var.version}-one-context.img"
    iso_url          = "${var.output_dir}/${var.version}/base/${local.output_name}-${var.version}.img"
    iso_checksum     = "none"
    disk_image       = true
    boot_command     = [ "<enter><enter><wait>" ]
  }

  provisioner "file" {
    destination = "/tmp/one-context.sh"
    source      = "${path.cwd}/provisionning/debian/one-context.sh"
  }

  provisioner "shell" {
    inline = [
      "sh -cx 'sudo bash /tmp/one-context.sh'"
    ]
  }

  provisioner "file" {
    destination = "/tmp/net-96-gitlab-register"
    source = "${path.cwd}/provisionning/one-context/net-96-gitlab-register"
  }

  provisioner "shell" {
    inline = [
      "sh -cx 'sudo cp /tmp/net-96-gitlab-register /etc/one-context.d/net-96-gitlab-register'"
    ]
  }

  post-processor "shell-local" {
    inline = [
      "/bin/sh ${path.cwd}/post-processors/sparsify.sh ${var.output_dir}/${var.version}/provisionned/one-context ${var.image_version}",
      "ruby ${path.cwd}/tools/one-templates -t image -T ${path.cwd}/templates/one/image/common.tpl -n ${local.output_name}-${var.version}-${build.name} -c '${local.output_name}-${var.version} (one-context)' --image-file ${var.output_dir}/${var.version}/provisionned/one-context/${local.output_name}-${var.version}-one-context.img"
    ]
  }
}
