# "timestamp" template function replacement
locals {
  timestamp = regex_replace(timestamp(), "[- TZ:]", "")
  output_directory = "output/ubuntu"
  output_name = "${var.name}"
  source_iso = "${var.source_url}/${var.code_name}/${var.image_dir_name}/${var.code_name}-server-cloudimg-${var.arch}.img"
  source_checksum = "file:${var.source_url}/${var.code_name}/current/SHA256SUMS"
  ssh_user = "packer"
  ssh_password = "cadoles"
  # Cloud-init instance data
  #"instance-id": "iid-local01"
  instance_data = {
     "instance-id": "${var.name}"
  }
}
