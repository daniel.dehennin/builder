name = "freebsd"
version = "12.3"
short_version = 12
arch = "amd64"

source_url = "http://ftp.freebsd.org/pub/FreeBSD/releases/ISO-IMAGES"
source_iso_name = "FreeBSD-12.3-RELEASE-amd64-disc1.iso"
iso_cd_checksum = "5dd048f8072086c58019c9e18042dc5716d19c2044e96daa2894ddac535712827cf2c8ffbc3098062ea5bfe372bb24f83cc07eae766e4a16a03e1308030af029"
boot_command = [
    "<esc><wait>",
    "boot -s<wait>",
    "<enter><wait>",
    "<wait50s>",
    "/bin/sh<enter><wait2s>",
    "mdmfs -s 100m md1 /tmp<enter><wait2s>",
    "mdmfs -s 100m md2 /mnt<enter><wait2s>",
    "dhclient -p /tmp/dhclient.vtnet0.pid -l /tmp/dhclient.lease.vtnet0 vtnet0<enter><wait>",
    "<wait5>",
    "fetch -o /tmp/installerconfig http://{{ .HTTPIP }}:{{ .HTTPPort }}/installerconfig && bsdinstall script /tmp/installerconfig && shutdown -r now<enter><wait>"
]