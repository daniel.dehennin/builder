#Flavour gitlab-runner
build {
  name = "gitlab-runner"
  description = <<EOF
This builder builds a QEMU image from the base build output. The goal here is to install gitlab-runner
with it's provisionning.
EOF
  source "source.qemu.freebsd" {
    output_directory = "${var.output_dir}/${var.version}/provisionned/gitlab-runner"
    iso_url      = "${var.output_dir}/${var.version}/base/${local.output_name}-${var.version}.img"
    iso_checksum = "none"
    disk_image = true
    vm_name = "${local.output_name}-${var.version}-gitlab-runner.img"
    disk_size   = 51200
  }

  provisioner "file" {
    destination = "/tmp/${build.name}.sh"
    source      = "${path.cwd}/provisionning/${var.name}/${build.name}.sh"
  }

  provisioner "file" {
    destination = "/tmp/gitlab_runner.rc.d"
    source = "${path.cwd}/provisionning/${var.name}/conf/gitlab_runner_rc.d.sh"
  }

  provisioner "file" {
    destination = "/tmp/one-context.sh"
    source      = "${path.cwd}/provisionning/${var.name}/one-context.sh"
  }

  provisioner "shell" {
    execute_command = "chmod +x {{ .Path }}; env {{ .Vars }} {{ .Path }}"
    inline = [
      "sh -cx 'sh /tmp/${build.name}.sh'",
      "sh -cx 'cp /tmp/gitlab_runner.rc.d /usr/local/etc/rc.d/gitlab_runner'",
      "sh -cx 'chmod +x /usr/local/etc/rc.d/gitlab_runner'",
      "sh -cx 'sysrc -f /etc/rc.conf \"gitlab_runner_enable=YES\"'",
      "sh -cx 'sh /tmp/one-context.sh'"
    ]
  }

  provisioner "file" {
    destination = "/tmp/net-96-gitlab-register"
    source = "${path.cwd}/provisionning/one-context/net-96-gitlab-register"
  }

  provisioner "shell" {
    execute_command = "chmod +x {{ .Path }}; env {{ .Vars }} {{ .Path }}"
    inline = [
      "sh -cx 'cp /tmp/net-96-gitlab-register /etc/one-context.d/net-96-gitlab-register'"
    ]
  }

  post-processor "shell-local" {
    inline = [
      "/bin/sh ${path.cwd}/post-processors/sparsify.sh ${var.output_dir}/${var.version}/provisionned/gitlab-runner ${var.image_version}",
      "ruby ${path.cwd}/tools/one-templates -t image -T ${path.cwd}/templates/one/image/common.tpl -n ${local.output_name}-${var.version}-${build.name} -c 'Gitlab runner on ${local.output_name}-${var.version}' --image-file ${var.output_dir}/${var.version}/provisionned/gitlab-runner/${local.output_name}-${var.version}-gitlab-runner.img"
    ]
  }
}
