#Flavour base
build {
  name = "base"
  description = <<EOF
This builder builds a QEMU image from a FreeBSD CD ISO file.
It contains a few basic tools and can be use as a "OpenNebula cloud image".
EOF

  source "qemu.freebsd" {
    output_directory = "${var.output_dir}/${var.version}/base"
    vm_name          = "${local.output_name}-${var.version}.img"
    iso_url          = "${local.source_iso}"
    iso_checksum     = "${var.iso_cd_checksum}"
    disk_size        = 51200
    boot_command     = var.boot_command
  }

  provisioner "file" {
    destination = "/tmp/freebsd.sh"
    source      = "${path.cwd}/provisionning/${var.name}/${var.name}-${var.short_version}.sh"
  }

  provisioner "shell" {
    execute_command = "chmod +x {{ .Path }}; env {{ .Vars }} {{ .Path }}"
    inline = [
      "sh /tmp/freebsd.sh"
    ]
  }

  post-processor "shell-local" {
    inline = [
      "/bin/sh ${path.cwd}/post-processors/sparsify.sh ${var.output_dir}/${var.version}/base ${var.image_version}",
      "ruby ${path.cwd}/tools/one-templates -t image -T ${path.cwd}/templates/one/image/common.tpl -n ${local.output_name}-${var.version} -c '${local.output_name}-${var.version} base image' --image-file ${var.output_dir}/${var.version}/base/${local.output_name}-${var.version}.img"
    ]
  }

  post-processor "manifest" {
    keep_input_artifact = true
  }
}
