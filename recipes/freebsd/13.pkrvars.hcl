name = "freebsd"
version = "13.0"
short_version = 13
arch = "amd64"

source_url = "http://ftp.freebsd.org/pub/FreeBSD/releases/ISO-IMAGES"
source_iso_name = "FreeBSD-13.0-RELEASE-amd64-disc1.iso"
iso_cd_checksum = "f78d4e5f53605592863852b39fa31a12f15893dc48cacecd875f2da72fa67ae5"
boot_command = [
    "<esc><wait>",
    "boot -s<wait>",
    "<enter><wait>",
    "<wait50s>",
    "/bin/sh<enter><wait2s>",
    "mdmfs -s 100m md1 /tmp<enter><wait2s>",
    "mdmfs -s 100m md2 /mnt<enter><wait2s>",
    "dhclient -p /tmp/dhclient.vtnet0.pid -l /tmp/dhclient.lease.vtnet0 vtnet0<enter><wait>",
    "<wait5>",
    "fetch -o /tmp/installerconfig http://{{ .HTTPIP }}:{{ .HTTPPort }}/installerconfig && bsdinstall script /tmp/installerconfig && shutdown -r now<enter><wait>"
]