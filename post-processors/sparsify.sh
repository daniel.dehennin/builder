#!/bin/sh

if [ "${#}" -ne 2 ]; then
  echo Missing arguments
  exit 2
fi

WORKDIR=${1}
VERSION=${2}

findImages() {
  find ${1} -iname "*.img"
}

sleep 5

for imageName in $(findImages ${WORKDIR} ${DOMAIN}); do
  if [ $(which virt-sparsify) ]; then
    newName=$(echo $imageName | sed "s/.img/_${VERSION}.img/g")
    virt-sparsify --compress --tmp ./ --format qcow2 ${imageName} ${newName}
    if [ "${?}" -eq 0 ]; then
      rm -rf ${imageName}
      cd ${WORKDIR}
      ln -s $(basename ${newName}) $(basename ${imageName})
      echo ${newName} ${imageName}
      cd -
    fi
  else
    echo "Sparsify skipped 'virt-sparsify' command is missing"
  fi
done
