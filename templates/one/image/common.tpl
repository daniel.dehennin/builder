NAME = <%= image_name %>
PATH = <%= image_source %>
TYPE = OS
PERSISTENT = No
DESCRIPTION = "<%= image_comment %>"
DEV_PREFIX = vd
FORMAT = qcow2