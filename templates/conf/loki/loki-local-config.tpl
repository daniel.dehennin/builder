{{ if .AuthEnabled  }}
auth_enabled: true
{{ else  }}
auth_enabled: false
{{ end  }}

server:
  http_listen_port: {{ .HTTPPort }}
  grpc_listen_port: {{ .GRPCPort }}
  log_level: {{ .LogLevel }}

ingester:
  wal:
    enabled: true
    dir: {{ .StorageRoot }}/wal
    flush_on_shutdown: true
  lifecycler:
    address: 127.0.0.1
    ring:
      kvstore:
        store: inmemory
      replication_factor: 1
    final_sleep: 0s
  chunk_idle_period: 1h       # Any chunk not receiving new logs in this time will be flushed
  max_chunk_age: 1h           # All chunks will be flushed when they hit this age, default is 1h
  chunk_target_size: 1048576  # Loki will attempt to build chunks up to 1.5MB, flushing first if chunk_idle_period or max_chunk_age is reached first
  chunk_retain_period: 30s    # Must be greater than index read cache TTL if using an index cache (Default index read cache TTL is 5m)
  max_transfer_retries: 0     # Chunk transfers disabled

schema_config:
  configs:
  - from: 2020-05-15
    store: boltdb-shipper
    object_store: {{ .ObjectStore }}
    schema: v11
    index:
      prefix: index_
      period: 24h

storage_config:
 boltdb_shipper:
   active_index_directory: {{ .StorageRoot }}/index
   shared_store: {{ .SharedStore }}
   cache_location: {{ .StorageRoot }}/cache
   cache_ttl: 168h

{{ if eq (.ObjectStore) ("filesystem") }}
  filesystem:
    directory: {{ .StorageRoot }}/chunks
{{ else  }}
  aws:
    s3: s3://{{ .S3.APIKey }}:{{ .S3.APISecretKey}}@{{ .S3.URL}}/{{ .S3.BucketName}}
    s3forcepathstyle: true
{{ end  }}

compactor:
  shared_store: {{ .SharedStore }}
  working_directory: {{ .StorageRoot }}/compactor
  compaction_interval: 10m

limits_config:
  reject_old_samples: true
  reject_old_samples_max_age: 168h

chunk_store_config:
  max_look_back_period: 0s

table_manager:
  retention_deletes_enabled: false
  retention_period: 0s

ruler:
  storage:
    type: local
    local:
      directory: {{ .StorageRoot }}/rules
  rule_path: {{ .StorageRoot }}/rules
  alertmanager_url: {{ .AlertManagerURL }}
  ring:
    kvstore:
      store: inmemory
  enable_api: true