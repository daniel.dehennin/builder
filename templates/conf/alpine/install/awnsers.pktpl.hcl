
# Example answer file for setup-alpine script
# If you don't want to use a certain option, then comment it out

# Use US layout with US variant
KEYMAPOPTS="fr fr"

# Set hostname to alpine-test
HOSTNAMEOPTS="-n ${hostname}"

# Contents of /etc/network/interfaces
INTERFACESOPTS="auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp
    hostname ${hostname}
"

# Search domain of example.com, OpenDNS public nameserver
# ex: -d example.com 1.1.1.1"
DNSOPTS=""

# Set timezone to UTC
TIMEZONEOPTS="-z Europe/Paris"

# set http/ftp proxy
PROXYOPTS="none"

# Add a random mirror
APKREPOSOPTS="-r -c"

# Install Openssh
SSHDOPTS="-c openssh -k /root/.ssh/authorized_keys"

# Use openntpd
NTPOPTS="-c openntpd"

# Use /dev/sda as a data disk
DISKOPTS="-L -m sys /dev/vda"

USEROPTS="-a -g 'netdev' ${user}"

# Setup in /media/vda1
# LBUOPTS="/media/vda1"
# APKCACHEOPTS="/media/vda1/cache"

