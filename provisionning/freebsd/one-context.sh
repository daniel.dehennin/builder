#!/bin/sh

ONE_CONTEXT_RELEASE="6.0.0"
ONE_CONTEXT_PKGVERSION="1"
ONE_CONTEXT_URL="https://github.com/OpenNebula/addon-context-linux/releases/download/"

cd /tmp

wget "${ONE_CONTEXT_URL}/v${ONE_CONTEXT_RELEASE}/one-context-${ONE_CONTEXT_RELEASE}_${ONE_CONTEXT_PKGVERSION}.txz"

pkg install -y curl bash sudo base64 ruby open-vm-tools-nox11
pkg install -y one-context-[0-9]*.txz

exit 0
