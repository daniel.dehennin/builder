#!/bin/sh

# Install gitlab-runner
ARCH=amd64
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash

apt-get install -y gitlab-runner
