#!/bin/sh

# Install qemu
apt-get install -y --no-install-recommends qemu-system software-properties-common curl

# Install packer
curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -
apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
apt-get update
apt-get install --no-install-recommends -y packer
