#!/bin/sh
#

echo "${1}" >/etc/hostname

apt-get update
apt-get -y dist-upgrade
apt-get install wget curl -y

touch /etc/cloud/cloud-init.disabled
