#!/bin/sh

apt-get update
apt-get install git -y

# Clone Provisionner
cd /tmp
git clone https://gitlab.mim-libre.fr/EOLE/eole-3/provisionner.git
cd provisionner
git checkout develop
echo "${USER}"
/bin/sh install-eolebase3.sh --server 1
