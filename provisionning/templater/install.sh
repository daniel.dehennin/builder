#!/bin/sh

set -ex

TOOL_DIR="${1:-/usr/local/bin}"
TOOL_USER="${2:-root}"
TOOL_GROUP="${3:-root}"
TOOL_NAME="tpr"
ATTACHMENT_ID="f5a43ed8-002f-4d01-9c72-a76216f573c7"
BIN_URL="https://forge.cadoles.com/attachments/${ATTACHMENT_ID}"

curl -k -o ${TOOL_DIR}/${TOOL_NAME} ${BIN_URL}
chmod +x ${TOOL_DIR}/${TOOL_NAME}
