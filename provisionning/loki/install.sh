#!/bin/sh

set -ex

LOKI_DIR="${1:-/opt/loki}"
LOKI_USER="${2:-loki}"
LOKI_GROUP="${3:-grafana}"

DISTRIB_ID=""
DISTRIB_RELEASE=""
if [ -f "/etc/alpine-release" ]; then
  DISTRIB_ID="alpine"
  DISTRIB_RELEASE=$(cat /etc/alpine-release)
elif [ -f "/etc/lsb-release" ]; then
  . /etc/lsb-release
elif [ -f "/etc/os-release" ]; then
  . /etc/os-release
  DISTRIB_ID="${ID}"
  DISTRIB_RELEASE="${VERSION_ID}"
fi

installUbuntu() {

  apt-get update
  apt-get install git -y

  # Creating Loki user and group
  groupadd "${LOKI_GROUP}"
  useradd -s /bin/sh -d "${LOKI_DIR}" -m -G grafana "${LOKI_USER}"

  # Installing loki
  LOKI_VERSION=$(curl -s "https://api.github.com/repos/grafana/loki/releases/latest" | grep -Po '"tag_name": "v\K[0-9.]+')
  #mkdir "${LOKI_DIR}"
  wget -qO "${LOKI_DIR}/loki.zip" "https://github.com/grafana/loki/releases/download/v${LOKI_VERSION}/loki-linux-amd64.zip"
  cd "${LOKI_DIR}"
  unzip loki.zip
  mv "${LOKI_DIR}/loki-linux-amd64" "${LOKI_DIR}/loki"
  chmod a+x "${LOKI_DIR}/loki"
  rm loki.zip
  ln -s ${LOKI_DIR}/loki /usr/local/bin/loki
  cd -

  # Installing logcli
  wget -qO "${LOKI_DIR}/logcli.zip" "https://github.com/grafana/loki/releases/download/v${LOKI_VERSION}/logcli-linux-amd64.zip"
  cd "${LOKI_DIR}"
  unzip logcli.zip
  mv "${LOKI_DIR}/logcli-linux-amd64" "${LOKI_DIR}/logcli"
  chmod a+x "${LOKI_DIR}/logcli"
  rm logcli.zip
  ln -s ${LOKI_DIR}/logcli /usr/local/bin/logcli
  cd -

  # Creating configuration directory
  mkdir -p /etc/loki

  # Create Loki Service

  cat >/etc/systemd/system/loki.service <<__EOF__
[Unit]
Description=Loki log aggregation system
After=network.target

[Service]
ExecStart=/opt/loki/loki -config.file=/etc/loki/loki-local-config.yaml
Restart=always

[Install]
WantedBy=multi-user.target
__EOF__

  service loki start
  systemctl enable loki
}

installAlpine() {
  apk update
  # Install Loki from edge
  apk add loki --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing
  groupadd -r ${LOKI_GROUP}
  useradd -d /dev/null -G ${LOKI_GROUP} -r ${LOKI_USER}
  rc-update add loki default
}

alias installDebian=installUbuntu

if [ "${DISTRIB_ID}" = "Ubuntu" ]; then
  installUbuntu
elif [ "${DISTRIB_ID}" = "debian" ]; then
  installDebian
elif [ "${DISTRIB_ID}" = "alpine" ]; then
  installAlpine
else
  echo "Unsupported distribution ${DISTRIB_ID}"
  exit 2
fi
