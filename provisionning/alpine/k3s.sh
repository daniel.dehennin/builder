#!/bin/sh

set -e

NAME="alk3s"
DEPS="curl coreutils"
BUILD_DEPS="git"
echo "${NAME}" >/etc/hostname
hostname -F /etc/hostname

apk update
apk add ${DEPS} ${BUILD_DEPS}

# Install OpenNebula k3s app
REPO_URL="https://gitlab.mim-libre.fr/EOLE/opennebula/k3s-cluster-app.git"
cd /tmp
git clone ${REPO_URL}
cd k3s-cluster-app
git checkout develop

echo "Starting K3S App setup !"
sh setup install
if [ "${?}" -eq 0 ]; then
  echo " = Stating Service install !"
  /etc/one-appliance/service install
  if [ "${?}" -ne 0 ]; then
    echo "Error running service install"
    return 3
  fi
  echo " = Service install is done !"
fi

echo "K3S App setup is done !"

apk del --purge ${BUILD_DEPS}
