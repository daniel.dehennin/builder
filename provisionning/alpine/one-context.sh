#!/bin/sh

set -e

ONE_CONTEXT_VERSION="6.2.0"
ONE_CONTEXT_PKG_VERSION="1"
PKG="one-context-${ONE_CONTEXT_VERSION}-r${ONE_CONTEXT_PKG_VERSION}.apk"
PKG_URL="https://github.com/OpenNebula/addon-context-linux/releases/download/v${ONE_CONTEXT_VERSION}/${PKG}"

cd /tmp || exit 3
wget -q --no-check-certificate ${PKG_URL}
apk add --allow-untrusted --no-cache ${PKG}
