#!/bin/sh

set -e

NAME="prometheus"
DEPS="prometheus"
BUILD_DEPS=""
echo "${NAME}" >/etc/hostname
hostname -F /etc/hostname

apk update

# Install deps and build deps
apk add ${DEPS} ${BUILD_DEPS}

# Enable service
rc-update add prometheus default

# Purge build deps
# apk del --purge ${BUILD_DEPS}
