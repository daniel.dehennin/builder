#!/bin/sh

set -e

NAME="${1: -loki}"
echo "${NAME}" >/etc/hostname
hostname -F /etc/hostname

apk update
apk upgrade
