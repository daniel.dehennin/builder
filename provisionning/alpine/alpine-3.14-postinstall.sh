#!/bin/sh
set -xeo pipefail

rlz=$(cut -d '.' -f1,2 /etc/alpine-release)

cat >/etc/apk/repositories <<EOF

https://dl-cdn.alpinelinux.org/alpine/v${rlz}/main/
https://dl-cdn.alpinelinux.org/alpine/v${rlz}/community/

EOF
$(echo)

apk add --no-cache wget curl jq haveged ca-certificates rsyslog

rc-update add haveged boot
rc-update add rsyslog boot
rc-update add sshd boot

# Generate root password
pass=$(openssl rand -base64 32 | tee -a .secret)
chmod 600 .secret
echo -e "${pass}\n${pass}" | passwd

# Remove expect package

# Prevent logs spamming like "process '/sbin/getty -L 0 ttyS0 vt100' (pid 2516) exited. Scheduling for restart."
# We don't need an access to ttyS0
sed -i 's@^\(ttyS0::respawn.*\)@#\1@' /etc/inittab

sync
